<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKoperasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koperasi', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('jenis_id')->unsigned();
            $table->foreign('jenis_id')
            ->references('id')->on('jenis_koperasi')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->tinyInteger('users_id')->unsigned();
            $table->foreign('users_id')
            ->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('nomor_spk');
            $table->date('tgl_spk');
            $table->string('nama_institusi',30);
            $table->string('nama_ketua',30);
            $table->text('alamat')->nullable();
            $table->mediumInteger('jml_anggota')->nullable();
            $table->string('provinsi');
            $table->string('kab');
            $table->integer('biaya_hosting');
            $table->string('pic_teknik');
            $table->string('no_telp',16)->nullable();
            $table->string('email',30);
            $table->string('url');
            $table->date('tgl_setup');
            $table->integer('harga_aplikasi');
            $table->string('history');
            $table->string('status_pembayaran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koperasi');
    }
}
