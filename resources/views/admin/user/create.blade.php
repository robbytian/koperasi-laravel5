@extends('layouts.app')
@section('title')
    Tambah User
@endsection
@section('headerPage')
    Tambah User
@endsection
@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('success') }}
</div>
@endif
@if(session()->has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('danger') }}
</div>
@endif
<form method="POST" action="{{route('user.store')}}">
    {{ csrf_field() }}
    <div class="form-group">
        <label>Role</label>
        <select name="role" class="form-control">
            @foreach ($role as $item)          
                <option value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Nama</label>
        <input type="name" class="form-control" name="name" placeholder="Masukan Nama" value="{{old('name')}}">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" name="email" placeholder="Masukan Email" value="{{old('email')}}">
    </div>
    <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control" name="password" placeholder="Masukan Password">
    </div>
    <div class="form-group">
        <label>Konfirmasi Password</label>
        <input type="password" class="form-control" name="password_confirmation" placeholder="Masukan Konfirmasi Password">
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>        

@endsection