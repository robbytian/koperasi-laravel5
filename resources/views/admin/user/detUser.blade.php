@extends('layouts.app')
@section('title')
    Data {{$dataUser->name}}
@endsection
@section('isi')
    <div class="m-portlet__body no-pedding">
        @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('danger'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                {{ session()->get('danger') }}
        </div>
        @endif
    </div>
@endsection
