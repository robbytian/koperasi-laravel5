@extends('layouts.app')
@section('title')
    Edit Koperasi
@endsection
@section('headerPage')
    Edit Koperasi
@endsection
@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('success') }}
</div>
@endif
@if(session()->has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('danger') }}
</div>
@endif
<form method="POST" action="{{route('koperasi.update',$koperasi->id)}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('put') }}
        <div class="form-group">
            <label>Jenis Koperasi</label>
            <select name="jenis_id" class="form-control">                
                @foreach ($jenis as $item)
                <option value="{{$item->id}}" {{ $item->id == $koperasi->jenis->id ? "selected" : "" }}>{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Nomor SPK</label>
            <input type="name" class="form-control" name="nomor_spk" placeholder="Masukan Nomor SPK" value="{{ $koperasi->nomor_spk }}">
        </div>
        <div class="form-group">
            <label class="form-control-label">Tanggal SPK</label>                
            <div class="input-group m-input-group">
                <input type="text" name="tgl_spk" class="form-control" id="m_datepicker_1" readonly="" placeholder="Select date &amp; time" value="{{ $koperasi->tgl_spk }}">
                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
            </div>
        </div>
        <div class="form-group">
            <label>Nama Institusi</label>
            <input type="name" class="form-control" name="nama_institusi" placeholder="Masukan Nama Institusi" value="{{ $koperasi->nama_institusi }}">
        </div>
        <div class="form-group">
            <label>Nama Ketua</label>
            <input type="name" class="form-control" name="nama_ketua" placeholder="Masukan Nama Ketua" value="{{ $koperasi->nama_ketua }}">
        </div>        
        <div class="form-group">
            <label for="">Alamat</label>
            <textarea name="alamat" cols="30" rows="10" class="form-control" placeholder="Masukan Alamat">{{ $koperasi->alamat }}</textarea>
        </div>
        <div class="form-group">
            <label>Jumlah Anggota</label>
            <input type="number" class="form-control" name="jml_anggota" placeholder="Masukan Jumlah Anggota" value="{{ $koperasi->jml_anggota }}">
        </div>
        <div class="form-group">
            <label>Provinsi</label>
            <select class="form-control" name="provinsi">
                @for($prov=0;$prov < count($provinsi); $prov++)
                    <option value="{{$provinsi[$prov]['province']}}" {{ $koperasi->provinsi == $provinsi[$prov]['province'] ? 'selected' : ''}}>{{$provinsi[$prov]['province']}}</option>
                @endfor
            </select>
        </div>
        <div class="form-group">
        <label>Kab</label>
            <select class="form-control" name="kab">
            @for($kab=0;$kab < count($kota); $kab++)
                    <option value="{{$kota[$kab]['city_name']}}" {{ $koperasi->kab == $kota[$kab]['city_name'] ? 'selected' : ''}}>{{$kota[$kab]['city_name']}}</option>
                @endfor
            </select>
        </div>
        <div class="form-group">
            <label>Biaya Hosting</label>
            <input type="number" class="form-control" name="biaya_hosting" placeholder="Masukan Biaya Hosting" value="{{ $koperasi->biaya_hosting }}">
        </div>
        <div class="form-group">
            <label>Pic Teknik</label>
            <input type="text" class="form-control" name="pic_teknik" placeholder="Masukan Pic Teknik" value="{{ $koperasi->pic_teknik }}">
        </div>
        <div class="form-group">
            <label>Nomor Telepon</label>
            <input type="name" class="form-control" name="no_telp" placeholder="Masukan Nomor Telepon" value="{{ $koperasi->no_telp }}">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" placeholder="Masukan Email" value="{{ $koperasi->email }}">
        </div>
        <div class="form-group">
            <label>URL</label>
            <input type="url" class="form-control" name="url" placeholder="Masukan URL" value="{{ $koperasi->url }}">
        </div>
        <div class="form-group">
            <label class="form-control-label">Tanggal Setup</label>
            <div class="input-group m-input-group">
                <input type="text" name="tgl_setup" class="form-control" id="m_datepicker_1" readonly="" placeholder="Select date &amp; time" value="{{ $koperasi->tgl_setup }}">
                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
            </div>
        </div>
        <div class="form-group">
            <label>Harga Aplikasi</label>
            <input type="number" class="form-control" name="harga_aplikasi" placeholder="Masukan Harga Aplikasi" value="{{ $koperasi->harga_aplikasi }}">
        </div>
        <div class="form-group">
            <label>History</label>
            <input type="text" class="form-control" name="history" placeholder="Masukan History" value="{{ $koperasi->history }}">
        </div>
        <div class="form-group">
            <label>Status Pembayaran</label>
            <select class="form-control" name="status_pembayaran">
                <option value="LUNAS" {{$koperasi->status_pembayaran =="LUNAS" ? 'selected' : ''}}>Lunas</option>
                <option value="BELUM LUNAS" {{$koperasi->status_pembayaran =="BELUM LUNAS" ? 'selected' : ''}}>Belum Lunas</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>        

@endsection