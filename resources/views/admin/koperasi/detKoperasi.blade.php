@extends('layouts.app')
@section('title')
  {{$dataKoperasi->nama_institusi}}
@endsection
@section('isi')
<div class="m-portlet m-portlet--head-lg">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon">
          <i class="la la-list"></i>
        </span>
        <h3 class="m-portlet__head-text">
        {{$dataKoperasi->nama_institusi}}
        </h3>
      </div>
    </div>
  </div>
  <div class="m-portlet__body no-pedding">
    @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {{ session()->get('success') }}
    </div>
    @endif
    @if(session()->has('danger'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {{ session()->get('danger') }}
    </div>
    @endif
    
  </div>
</div>
@endsection
