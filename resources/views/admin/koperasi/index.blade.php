@extends('layouts.app')
@section('title')
    Daftar Semua Kategori
@endsection
@section('headerPage')
    Daftar Semua Kategori
@endsection
@section('isi')
<div class="m-portlet m-portlet--head-lg">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="la la-list"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    Daftar Semua Kategori
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="{{ url('jenis_koperasi/create') }}" class="btn m-btn btn-primary btn-sm m-btn--icon m-btn--pill m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Jenis</span>
                        </span>
                    </a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="{{ url('koperasi/create') }}" class="btn m-btn btn-success btn-sm m-btn--icon m-btn--pill m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Koperasi</span>
                        </span>
                    </a>
                </li>        

            </ul>
        </div>
        
    </div>
    <div class="m-portlet__body no-pedding">
        @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('danger'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                {{ session()->get('danger') }}
        </div>
        @endif
        <div class="list-section">                
            @forelse ($jenisKoperasi as $item)
            @php 
                $nama = \App\jenisKoperasiModel::where('id',$item->id)->first();
            @endphp
            <div class="list-section__item">
                <div class="section__content image-box">
					<div class="section__widget">
						<div class="section__img">
							<img src="{{asset('jenisImage/'.$item->image)}}" class="imageThumb">
						</div>
					</div>
                    <div class="section__desc">
                        <h5 class="section__title">{{ $item->nama }}</h5>
                        <div class="section__info">

                            <div class="section__info__item sm-text">
                                <span class="info__label">Posted By :</span>
                                <a href="" class="info__detail m-link">{{ $item->users->name }}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section__action">
                    <div class="list__section__action">
						<a href="{{ url('koperasi/'.$item->id) }}"
                            class="btn m-btn btn-info btn-sm  m-btn--icon m-btn--square m-btn--air icon-only">
                            <span>
                                <i class="la la-list"></i>
                                <span>Lihat Koperasi</span>
                            </span>
                        </a>
                        <a href="{{ url('jenis_koperasi/'.$item->id.'/edit') }}" class="btn m-btn btn-success btn-sm m-btn--icon m-btn--air icon-only">
                            <span>
                                <i class="la la-pencil"></i>
                                <span>Edit Kategori</span>
                            </span>
                        </a>
                        <!-- <a href="#" class="btn m-btn btn-primary btn-sm m-btn--icon m-btn--pill m-btn--air icon-only">
                            <span>
                                <i class="la la-eye"></i>
                                <span>Lihat Koperasi</span>
                            </span>
                        </a> -->
                        <button class="btn m-btn btn-outline-danger btn-sm  m-btn--icon m-btn--pill icon-only m_sweetalert_5 deleteJenisKoperasi" data-id="{{$item->id}}" data-nama="{{$nama->nama}}">
                           
                            <span>
                                <i class="la la-trash"></i>
                                <span>Delete Kategori</span>
                            </span></button>
                        
                    </div>
                </div>
            </div>
            @empty
            <div class="m-portlet__body">
                <p>Data Kosong</p>
            </div>
            @endforelse            
        </div>
    </div>
</div>
@endsection

@section('modals')
<div class="modal fade" id="deleteJenisKoperasi"  role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
        <form action="{{ route('jenisKoperasi.destroy' ) }}" method="post">
               {{ method_field('delete') }}
               {{ csrf_field() }}
            <div class="modal-header">
                <h5 class="modal-title text-center" id="editModalLabel">Hapus Jenis Koperasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-center" id="nama_Jeniskoperasi" class="text-center">Hapus Koperasi Jenis</p>
                <input type="hidden", name="konfirmasiDelete" id="app_id">
            </div>
            <div class="modal-footer">
                <button type="close" class="btn m-btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn m-btn btn-danger"> Hapus </a>
            </div> 
            </form>
        </div>
    </div>
</div>
@endsection