@extends('layouts.app')
@section('title')
  Daftar Semua Koperasi
@endsection
@section('isi')
<div class="m-portlet m-portlet--head-lg">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon">
          <i class="la la-list"></i>
        </span>
        <h3 class="m-portlet__head-text">
          Daftar Semua Koperasi
        </h3>
      </div>
    </div>
  </div>
  <div class="m-portlet__body no-pedding">
    @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {{ session()->get('success') }}
    </div>
    @endif
    @if(session()->has('danger'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {{ session()->get('danger') }}
    </div>
    @endif
    <div class="list-section">
      
      <div class="m-portlet__body">
      <div class="kt-section__content">
        <table class="table">
            <thead>
              <tr>
                  <th>#</th>
                  <th>Nama Koperasi</th>
                  <th>Jenis Koperasi</th>
                  <th>Provinsi</th>
                  <th>Kab/Kota</th>
                  <th>Jumlah Anggota</th>
                  <th>Tanggal Buat</th>
                  <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @php
                $i = 1;
              @endphp
              @forelse ($koperasi as $item) 
              <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{ $item->nama_institusi}}</td>
                <td>{{ $item->jenis->nama}}</td>
                <td>{{ $item->provinsi}}</td>
                <td>{{ $item->kab}}</td>
                <td>{{ $item->jml_anggota}}</td>
                <td>{{ $item->tgl_setup}}</td>
                <td><a href="{{ url('detKoperasi/'.$item->id) }} " class="btn m-btn btn-primary btn-sm m-btn--icon m-btn--pill m-btn--air icon-only">
                    <span>
                      <i class="la la-eye"></i>
                      <span>Profil Koperasi</span>
                    </span>
                  </a></td>
                
              </tr>
              @empty
              @endforelse
            </tbody>
        </table>
      </div>
      </div>      
    </div>
  </div>
</div>
@endsection