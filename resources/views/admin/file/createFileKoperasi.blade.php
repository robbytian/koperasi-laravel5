@extends('layouts.app')
@section('title')
    Tambah File
@endsection
@section('headerPage')
    Tambah File
@endsection
@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('success') }}
</div>
@endif
@if(session()->has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('danger') }}
</div>
@endif
<form method="POST" action="{{url('file')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="">Pilih Koperasi</label>
        <select name="koperasi_id" class="form-control">
            @foreach ($koperasi as $item)
                <option value="{{$item->id}}">{{ $item->nama_institusi }}</option>
            @endforeach
            
        </select>
    </div>
    <div class="form-group control-group increment">
        <label class="form-control-label">Upload Dokumen</label>
        <div class="input-group control-group">                
            <input type="file" name="nama[]" id="file-input" onchange="return FileKoperasi(this)" class="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,image/*,.doc,.docx,application/msword" required>
            <div class="input-group-btn"> 
                <button class="btn btn-success addFile" type="button"><i class="la la-plus-square"></i></button>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary" style="margin-top:40px;" >Simpan</button>
</form>        

@endsection