@extends('layouts.app')
@section('title')
    Edit File
@endsection
@section('headerPage')
    Edit File {{$koperasi->nama}}
@endsection
@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('success') }}
</div>
@endif
@if(session()->has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('danger') }}
</div>
@endif
<form method="POST" action="{{url('file/'.$koperasi->id)}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('put') }}
    <div class="form-group control-group">
        <label class="form-control-label">Upload Dokumen</label>
        <div class="input-group control-group">                
            <input type="file" id="editFile" name="nama" class="form-control"  onchange="return FileKoperasi(this)" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,image/*,.doc,.docx,application/msword">
            <div class="input-group-btn"> 
                <button class="btn btn-primary" type="button"><i class="la la-file"></i></button>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary" style="margin-top:40px;" >Simpan</button>
</form>        

@endsection