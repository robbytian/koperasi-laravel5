@extends('layouts.app')
@section('title')
    Tambah Jenis Koperasi
@endsection
@section('headerPage')
    Tambah Jenis Koperasi
@endsection
@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('success') }}
</div>
@endif
@if(session()->has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('danger') }}
</div>
@endif
<form method="POST" action="{{route('jenis_koperasi.store')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
        <div class="form-group">
            <label>Nama</label>
            <input type="name" class="form-control" name="nama" placeholder="Masukan Nama Jenis Kopeasi" required>
        </div>
        <div class="form-group m-form__group row">
                <label class="form-control-label col-sm-12">Foto</label>
                <div class="col-sm-12">
                    <div class="input-group file-input" onchange="return FotoJenisKoperasi(event,foto,nama_foto,previewFoto_insert)">
                        <input type="file" name="image" id="foto"  class="upload" accept="image/*" required>
                        <input class="form-control" placeholder="No File Selected" type="text" id="nama_foto">
                        <span class="remove-file" style="display: none;"><i class="la la-times-circle"></i></span>
                        <div class="input-group-append">
                            <button type="button" class="btn btn-primary file-name"><i class="la la-file"></i></button>
                        </div>		
                    </div>
                    <img id="previewFoto_insert" src="" class="imgJenisKoperasi" style="display:none"/>
                </div>  
        </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
</form>        

@endsection
