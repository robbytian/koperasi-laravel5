@extends('layouts.app')
@section('title')
    Edit Jenis Koperasi
@endsection
@section('headerPage')
    Edit Jenis Koperasi
@endsection
@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('success') }}
</div>
@endif
@if(session()->has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('danger') }}
</div>
@endif
<form method="POST" action="{{route('jenis_koperasi.update',$jenisKoperasi->id)}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" value="{{$jenisKoperasi->nama}}">            
        </div>
        <div class="form-group m-form__group row">
                <label class="form-control-label col-sm-12">File Input</label>
                <div class="col-sm-12">
                    <div class="input-group file-input" onchange="return FotoJenisKoperasi(event,foto,nama_foto,previewFoto_edit)">
                        <input type="file" name="image" id="foto"  class="upload" accept="image/*">
                        <input class="form-control" id="nama_foto" placeholder="No File Selected" type="text" value="{{$jenisKoperasi->image}}">
                        <span class="remove-file" style="display: none;"><i class="la la-times-circle"></i></span>
                        <div class="input-group-append">
                            <button type="button" class="btn btn-primary file-name"><i class="la la-file"></i></button>
                        </div>		
                    </div>
                    <img id="previewFoto_edit" src="{{asset('jenisImage/'.$jenisKoperasi->image)}}" class="imgJenisKoperasi" style="display:block" />
                </div>
            </div> <button type="submit" class="btn btn-primary">Simpan</button>
</form>        

@endsection