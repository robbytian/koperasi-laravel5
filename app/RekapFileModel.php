<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekapFileModel extends Model
{
    protected $table = 'rekap_file';
    protected $fillable = [
        'koperasi_id','users_id','nama'
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public function koperasi()
    {
        return $this->belongsTo('App\KoperasiModel');
    }
}
