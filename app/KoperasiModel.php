<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class KoperasiModel extends Model
{
    use HasRoles;

    protected $guard_name = 'web'; // or whatever guard you want to use
    protected $table = 'koperasi';
    protected $fillable = [
        'jenis_id','users_id','nomor_spk','tgl_spk','nama_institusi','nama_ketua','alamat','jml_anggota','provinsi','kab','biaya_hosting','pic_teknik','no_telp','email','url','tgl_setup','harga_aplikasi','history','status_pembayaran'
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public function jenis()
    {
        return $this->belongsTo('App\JenisKoperasiModel');
    }
    public function rekap_file()
    {
        return $this->hasMany('App\RekapFileModel','koperasi_id');
    }
}
