<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KoperasiRequestStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jenis_id' => 'required',
            'nomor_spk' => 'required',
            'tgl_spk' => 'required|date_format:Y-m-d',
            'nama_institusi' => 'required',
            'nama_ketua' => 'required',
            'no_telp' => 'required|numeric|min:11',
            'url' => 'required|url',
            'tgl_setup' => 'required|date_format:Y-m-d',
            'harga_aplikasi' => 'required|numeric',
        ];
    }
    public function attributes()
    {
        return [
            'jenis_id' => 'Jenis Koperasi',
            'nomor_spk' => 'Nomor SPK',
            'tgl_spk' => 'Tanggal SPK',
            'nama_institusi' => 'Nama Institusi',
            'nama_ketua' => 'Nama Ketua',
            'no_telp' => 'Nomor Telepon',
            'url' => 'URL',
            'tgl_setup' => 'Tanggal Setup',
            'harga_aplikasi' => 'Harga Aplikasi'
        ];
    }
    public function messages()
    {
        return [
            'jenis_id.required' => ':attribute Harus di Isi',
            'nomor_spk.required' => ':attribute Harus di Isi',
            'tgl_spk.required' => ':attribute Harus di Isi',
            'tgl_spk.date' => ':attribute Tidak Sesuai Dengan Format',
            'nama_institusi.required' => ':attribute Harus di Isi',
            'nama_ketua.required' => ':attribute Harus di Isi',
            'no_telp.required' => ':attribute Harus di Isi',
            'no_telp.numeric' => ':attribute Harus Angka',
            'no_telp.min' => ':attribute Minimal Harus 11 Nomor',
            'url.url' => ':attribute Tidak Sesuai Dengan Format',
            'url.required' => ':attribute Harus di Isi',
            'tgl_setup.required' => ':attribute Harus di Isi',
            'tgl_setup.date' => ':attribute Tidak Sesuai Dengan Format',
            'harga_aplikasi.required' => ':attribute Harus di Isi',
            'harga_aplikasi.numeric' => ':attribute Harus Angka'
        ];
    }
}
