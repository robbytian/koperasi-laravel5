<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KoperasiModel;
use App\JenisKoperasiModel;
use App\RekapFileModel;
use Auth;
use File;
use App\Http\Requests\KoperasiRequestStore;

class KoperasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uniqueFilename($path, $name, $ext) {
	
        $output = $name;
        $basename = basename($name, '.' . $ext);
        $i = 2;
        
        while(File::exists($path . '/' . $output)) {
            $output = $basename . $i . '.' . $ext;
            $i ++;
        }
        
        return $output;
        
    }
    public function index()
    {
        $jenisKoperasi = JenisKoperasiModel::all();
        return view('admin.koperasi.index',['jenisKoperasi' => $jenisKoperasi]);
    }
    public function allKoperasi()
    {
        $koperasi = KoperasiModel::with(['users','jenis'])->get();        
        return view('admin.koperasi.allKoperasi',['koperasi' => $koperasi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://api.rajaongkir.com/starter/province',
            array(
                'headers' => array(
                    'key' => '993596248bfbf46f6bae821e466878ac',
                )
            )
        );
        $response = $request->getBody()->getContents();
        $result= json_decode($response,true);

        $request2 = $client->get('http://api.rajaongkir.com/starter/city',
            array(
                'headers' => array(
                    'key' => '993596248bfbf46f6bae821e466878ac',
                )
            )
        );
        $response2 = $request2->getBody()->getContents();
        $result2= json_decode($response2,true);

        $data['provinsi'] = $result['rajaongkir']['results'];
        $data['kota'] = $result2['rajaongkir']['results'];

        $data['jenisKoperasi'] = JenisKoperasiModel::all();
        return view('admin.koperasi.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KoperasiRequestStore $request)
    {
        try {            
            $koperasi = new KoperasiModel;
            $koperasi->jenis_id = $request->input('jenis_id');
            $koperasi->users_id = Auth::user()->id;
            $koperasi->nomor_spk = $request->input('nomor_spk');
            $koperasi->tgl_spk = $request->input('tgl_spk');
            $koperasi->nama_institusi = $request->input('nama_institusi');
            $koperasi->nama_ketua = $request->input('nama_ketua');
            $koperasi->alamat = $request->input('alamat');
            $koperasi->jml_anggota = $request->input('jml_anggota');
            $koperasi->provinsi = $request->input('provinsi');
            $koperasi->kab = $request->input('kab');
            $koperasi->biaya_hosting = $request->input('biaya_hosting');
            $koperasi->pic_teknik = $request->input('pic_teknik');
            $koperasi->no_telp = $request->input('no_telp');
            $koperasi->email = $request->input('email');
            $koperasi->url = $request->input('url');
            $koperasi->tgl_setup = $request->input('tgl_setup');
            $koperasi->harga_aplikasi = $request->input('harga_aplikasi');
            $koperasi->history = $request->input('history');
            $koperasi->status_pembayaran = $request->input('status_pembayaran');
            $koperasi->save();
            
            return redirect('koperasi/'.$koperasi->jenis_id)->with('success','Data Berhasil di Masukan');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jenisKoperasi = JenisKoperasiModel::with(['koperasi','users'])->findOrFail($id);        
        return view('admin.koperasi.show',['jenisKoperasi' => $jenisKoperasi]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://api.rajaongkir.com/starter/province',
            array(
                'headers' => array(
                    'key' => '993596248bfbf46f6bae821e466878ac',
                )
            )
        );
        $response = $request->getBody()->getContents();
        $result= json_decode($response,true);

        $request2 = $client->get('http://api.rajaongkir.com/starter/city',
            array(
                'headers' => array(
                    'key' => '993596248bfbf46f6bae821e466878ac',
                )
            )
        );
        $response2 = $request2->getBody()->getContents();
        $result2= json_decode($response2,true);

        $data['koperasi'] = KoperasiModel::with(['jenis','rekap_file'])->findOrFail($id);
        $data['provinsi'] = $result['rajaongkir']['results'];
        $data['kota'] = $result2['rajaongkir']['results'];
        $data['jenis'] = JenisKoperasiModel::all();        
        return view('admin.koperasi.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KoperasiRequestStore $request, $id)
    {
        try {
            $koperasi = KoperasiModel::with('jenis')->findOrFail($id);
            $koperasi->jenis_id = $request->input('jenis_id');
            $koperasi->users_id = Auth::user()->id;
            $koperasi->nomor_spk = $request->input('nomor_spk');
            $koperasi->tgl_spk = $request->input('tgl_spk');
            $koperasi->nama_institusi = $request->input('nama_institusi');
            $koperasi->nama_ketua = $request->input('nama_ketua');
            $koperasi->alamat = $request->input('alamat');
            $koperasi->jml_anggota = $request->input('jml_anggota');
            $koperasi->provinsi = $request->input('provinsi');
            $koperasi->kab = $request->input('kab');
            $koperasi->biaya_hosting = $request->input('biaya_hosting');
            $koperasi->pic_teknik = $request->input('pic_teknik');    
            $koperasi->no_telp = $request->input('no_telp');
            $koperasi->email = $request->input('email');
            $koperasi->url = $request->input('url');
            $koperasi->tgl_setup = $request->input('tgl_setup');
            $koperasi->harga_aplikasi = $request->input('harga_aplikasi');
            $koperasi->history = $request->input('history');
            $koperasi->status_pembayaran = $request->input('status_pembayaran');

            $koperasi->save();

            return redirect('koperasi/'.$koperasi->jenis->id)->with('success', 'Data Berhasil di Perbaharui');         
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }


    public function destroy(Request $request)
    {
        try {
            $id = $request->input('konfirmasiDelete');
            $koperasi = KoperasiModel::with('rekap_file')->findOrFail($id);
            foreach($koperasi->rekap_file as $value)
            {
                $path = public_path()."/files/".$value->nama;
                File::delete($path);
            }            
            $koperasi->delete();

            return back()->with('success','Data Berhasil di Hapus');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }

    public function detKoperasi($id){
        $data['dataKoperasi'] = KoperasiModel::findOrFail($id);
        return view('admin.koperasi.detKoperasi')->with($data);
    }
}
