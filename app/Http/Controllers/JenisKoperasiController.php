<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Auth;
use Image;
use App\JenisKoperasiModel;

class JenisKoperasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jenisKoperasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if($request->hasfile('image'))
            {
                $image = $request->file('image');            
                $thumbnailImage = Image::make($image);            
                $originalPath = public_path().'/jenisImage/';
                $thumbnailImage->save($originalPath.time().$image->getClientOriginalName());
                $jenis = new JenisKoperasiModel;
                $jenis->users_id = Auth::user()->id;
                $jenis->nama = $request->input('nama');
                $jenis->image = time().$image->getClientOriginalName();
                $jenis->save();

                return redirect('koperasi')->with('success','Data Berhasil di Tambahkan');
            } else {
                $jenis = new JenisKoperasiModel;
                $jenis->users_id = Auth::user()->id;
                $jenis->nama = $request->input('nama');
                $jenis->image = 'default_image.jpg';
                $jenis->save();
                return redirect('koperasi')->with('success','Data Berhasil di Tambahkan');
            }
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenisKoperasi = JenisKoperasiModel::findOrFail($id);
        return view('admin.jenisKoperasi.edit',[     
            'jenisKoperasi' => $jenisKoperasi
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if($request->hasFile('image'))
            {
                $image = $request->file('image');
                $thumbnailImage = Image::make($image);
                $originalPath = public_path().'/jenisImage/';
                $thumbnailImage->save($originalPath.time().$image->getClientOriginalName());
                $jenis = JenisKoperasiModel::findOrFail($id);
                $deletedImageOld = public_path().'/jenisImage/'.$jenis->image;
                File::delete($deletedImageOld);
                $jenis->users_id = Auth::user()->id;
                $jenis->nama = $request->input('nama');
                $jenis->image = time().$image->getClientOriginalName();
                $jenis->save();

                return redirect('koperasi')->with('success', 'Data Berhasil di Perbaharui');
            } else {
                $jenis = JenisKoperasiModel::findOrFail($id);                
                $jenis->users_id = Auth::user()->id;
                $jenis->nama = $request->input('nama');
                $jenis->save();
                return redirect('koperasi')->with('success', 'Data Berhasil di Perbaharui');
            }

        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }

    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('konfirmasiDelete');
            $jenis = JenisKoperasiModel::findOrFail($id);
            if($jenis->image == "default_image.jpg")
            {                
                $jenis->delete();
            } else {
                $deleteImage = public_path()."/jenisImage/".$jenis->image;            
                File::delete($deleteImage);
                $jenis->delete();
            }
            return back()->with('success','Data Berhasil di Hapus');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
}
