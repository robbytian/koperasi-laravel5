<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KoperasiModel;
use App\RekapFileModel;
use File;
use Auth;
use App\Http\Requests\FileRequestStore;
class FileController extends Controller
{
    public function uniqueFilename($path, $name, $ext) {
	
        $output = $name;
        $basename = basename($name, '.' . $ext);
        $i = 2;
        
        while(File::exists($path . '/' . $output)) {
            $output = $basename . $i . '.' . $ext;
            $i ++;
        }
        
        return $output;
        
    }    

    public function createFileKoperasi()
    {
        $koperasi = KoperasiModel::orderBy('nama_institusi')->get();
        return view('admin.file.createFileKoperasi',['koperasi' => $koperasi]);
    }
    public function showFileKoperasi($id)
    {
        $koperasi = KoperasiModel::with(['rekap_file','users'])->findOrFail($id);
        return view('admin.file.showFileKoperasi',['koperasi' => $koperasi]);
    }
    public function storeFile(FileRequestStore $request)
    {
        try {
            foreach($request->file('nama') as $file)
            {  
                $destination = public_path() . '/files';
                $name = $this->uniqueFilename($destination, $file->getClientOriginalName(), $file->getClientOriginalExtension());
                $file->move($destination, $name);
                $rekapFile = new RekapFileModel;
                $rekapFile->koperasi_id = $request->input('koperasi_id');
                $rekapFile->users_id = Auth::user()->id;
                $rekapFile->nama = $name;
                $rekapFile->save();
            }    
            return redirect('/file/'.$rekapFile->koperasi_id)->with('success','File Berhasil Dimasukan');
        } catch (Exception $e) {
            return back()->with('danger',$e->getMessage());
        }    
    }
    public function destroyFileKoperasi(Request $request)
    {
        try {            
            $id = $request->input('konfirmasiDelete');
            $koperasi = RekapFileModel::findOrFail($id);
            $pathFile = public_path()."/files/".$koperasi->nama;
            File::delete($pathFile);
            $koperasi->delete();

            return back()->with('success','Data Berhasil di Hapus');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
    public function editFileKoperasi($id)
    {
        $koperasi = RekapFileModel::findOrFail($id);        
        return view('admin.file.editFileKoperasi',['koperasi' => $koperasi]);
    }
    public function updateFileKoperasi(Request $request,$id)
    {
        try{
            $file = $request->file('nama');
            $destination = public_path() . '/files';
            $name = $this->uniqueFilename($destination, $file->getClientOriginalName(), $file->getClientOriginalExtension());
            $file->move($destination, $name);
            $koperasi = RekapFileModel::findOrFail($id);
            $oldFile = public_path().'/files/'.$koperasi->nama;
            File::delete($oldFile);
            $koperasi->koperasi_id = $koperasi->koperasi_id;
            $koperasi->users_id = Auth::user()->id;
            $koperasi->nama = $name;
            $koperasi->save();
            return redirect('/file/'.$koperasi->koperasi_id)->with('success','File Berhasil Diperbaharui');
        }
        catch(Exception $e){
            return back()->with('danger',$e->getMessage());
        }

    }
}
