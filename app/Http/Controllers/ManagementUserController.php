<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;
use Auth;
use Hash;
class ManagementUserController extends Controller
{
    public function formRole()
    {
        $data['roles'] = Role::all();
        return view('admin.user.formRole')->with($data);
    }
    public function addRole(Request $request) {
        try {
            $role = Role::create(['name' => $request->input('name')]);        
            return back()->with('success', 'Data Berhasil di Tambahkan');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
    public function allUsers()
    {        
        $user = User::where('id', '!=', Auth::user()->id)->with('roles')->get();
        return view('admin.user.index',['user' => $user]);
    }
    public function createUser()
    {
        $role = Role::all();
        return view('admin.user.create',['role' => $role]);
    }
    public function storeUser(Request $request)
    {
        try {
            if($request->input('password') != $request->input('password_confirmation')){
                return back()->with('danger','Password dan Password Konfirmasi Harus Sama')->withInput();
            }
            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password'))
            ]);
            $user->assignRole($request->input('role'));
            return redirect('/user')->with('success','User Berhasil Dibuat');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
    public function editUser($id)
    {
        $user = User::with('roles')->findOrFail($id);        
        $role = Role::all();        
        return view('admin.user.edit',['user' => $user, 'role' => $role]);
    }
    public function updateUser(Request $request)
    {
        try {
            $user = User::findOrFail($request->input('id'));        
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->syncRoles($request->input('role'));
            $user->save();

            return redirect('user')->with('success','Data Berhasil di Update');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
    public function destroyUser(Request $request)
    {
        try {
            $id = $request->input('konfirmasiDelete');
            $user = User::findOrFail($id);
            $user->delete();

            return back()->with('sucess','Data Berhasil di Hapus');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }

    public function detUser($id){
        $data['dataUser'] = User::findOrFail($id);
        return view('admin.user.detUser')->with($data);
    }
}
