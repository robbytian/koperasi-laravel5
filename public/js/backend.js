/*Add Form Input */
$(document).ready(function() {
  var i=1; 
  $(".addFile").click(function(){ 
    i++;  
    $('.increment').append( 
      '<div class="clone hide" id="row'+i+'">'+
      '<div class="control-group input-group" style="margin-top:10px">'+
          '<input type="file" name="nama[]" class="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,image/*">'+
          '<div class="input-group-btn">'+
              '<button class="btn btn-danger" id="'+i+'" type="button"><i class="la la-minus-square"></i></button>'+
          '</div>'+
      '</div>'+
  '</div>'
    );  
  });

  $("body").on("click",".btn-danger",function(){ 
    var button_id = $(this).attr("id");   
    $('#row'+button_id+'').remove();
    i--;
  });

});
/*End Add Form Input */

function FileKoperasi(fileInput){
  var filePath = fileInput.value;
  var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
  if(ext == "jpg"  || ext == "JPG"|| ext == "png" || ext=="jpeg" || ext == "gif" || ext == "xlsx" || ext == "xls" || ext == "doc" || ext == "docx")
  {
      return true;
  }else
  {
      alert("Hanya Bisa Memasukan Gambar, File Excel, dan File Word");
      fileInput.focus();
      fileInput.value='';
      return false;
  }
}

function FotoJenisKoperasi(event,fileInput,fileName,previewImage){
  var filePath = fileInput.value;
  var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
  if(ext == "jpg"  || ext == "JPG"|| ext == "png" || ext=="jpeg" || ext == "gif")
  {
      previewImage.src = URL.createObjectURL(event.target.files[0]);
      previewImage.style.display = "block";
      return true;
  }else
  {
      alert("Hanya Bisa Memasukan Gambar");
      fileInput.focus();
      fileInput.value='';
      fileName.value='';
      return false;
  }
}

$(document).on('click','.deleteJenisKoperasi',function(){
  var id=$(this).attr('data-id');
  var nama = $(this).attr('data-nama');
  $('#app_id').val(id); 
  $('#nama_Jeniskoperasi').text('Hapus Koperasi Jenis "'+nama+'"'+' ?');
  $('#deleteJenisKoperasi').modal('show'); 
});

$(document).on('click','.deleteUser',function(){
  var id=$(this).attr('data-id_user');
  var nama = $(this).attr('data-nama_user');
  $('#app_id').val(id); 
  $('#nama_user').text('Hapus User "'+nama+'"'+' ?');
  $('#deleteUserModal').modal('show'); 
});

$(document).on('click','.deleteKoperasi',function(){
  var id=$(this).attr('data-id_koperasi');
  var koperasi = $(this).attr('data-nama_koperasi');
  $('#app_id').val(id); 
  $('#nama_koperasi').text('"'+koperasi+'"'+' ?');
  $('#deleteKoperasi').modal('show'); 
});

$(document).on('click','.deleteFile',function(){
  var id=$(this).attr('data-id_file');
  var file = $(this).attr('data-nama_file');
  $('#app_id').val(id); 
  $('#nama_file').text('"'+file+'"'+' ?');
  $('#deleteFile').modal('show'); 
});


