<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('register.reset');
Route::get('password/reset/','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('register','Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register','Auth\RegisterController@register');
Route::post('login','Auth\LoginController@login');
Route::get('login','Auth\LoginController@showLoginForm')->name('login');


Route::group(['middleware' => ['auth']], function () {
    Route::post('logout','Auth\LoginController@logout')->name('logout');
    Route::resource('koperasi','KoperasiController')->except(['destroy']);;
    Route::delete('koperasi/delete','KoperasiController@destroy')->name('koperasi.destroy');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('jenis_koperasi','JenisKoperasiController')->except(['destroy']);
    Route::delete('jenis_koperasi/delete','JenisKoperasiController@destroy')->name('jenisKoperasi.destroy');
    Route::get('role','ManagementUserController@formRole')->name('role');
    Route::post('role','ManagementUserController@addRole');
    Route::get('user','ManagementUserController@allUsers')->name('user');
    Route::get('user/create','ManagementUserController@createUser')->name('user.create');
    Route::post('user','ManagementUserController@storeUser')->name('user.store');
    Route::delete('user','ManagementUserController@destroyUser')->name('user.destroy');
    Route::get('user/{id}/edit','ManagementUserController@editUser')->name('user.edit');
    Route::put('user/{id}','ManagementUserController@updateUser')->name('user.update');
    Route::get('file/create','FileController@createFileKoperasi')->name('file.create');
    Route::post('file','FileController@storeFile')->name('file.store');
    Route::get('file/{id}','FileController@showFileKoperasi')->name('fileKoperasi.show');
    Route::delete('file/delete','FileController@destroyFileKoperasi')->name('fileKoperasi.destroy');
    Route::get('file/{id}/edit','FileController@editFileKoperasi');
    Route::put('file/{id}','FileController@updateFileKoperasi');
    Route::get('allKoperasi','KoperasiController@allKoperasi')->name('all.koperasi');
    Route::get('detKoperasi/{id}','KoperasiController@detKoperasi')->name('det.koperasi');
    Route::get('detUser/{id}','ManagementUserController@detUser')->name('det.user');
});
